package com.vendingmachine.controlpanel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;

import org.junit.Test;

import com.vendingmachine.email.EmailSender;
import com.vendingmachine.machine.IVendingMachine;
import com.vendingmachine.machine.VendingMachineService;
import com.vendingmachine.machine.product.CocaCola;
import com.vendingmachine.machine.product.Product;
import com.vendingmachine.machine.storage.Storage;
import com.vendingmachine.report.PDFGenerator;

public class ControlPanelTest {

	@Test
	public void dispenceProductTest() {
		VendingMachineService iVendingMachine = new VendingMachineService();
		Storage storage = new Storage();
		Map<Integer, Queue<Product>> storageMap = new HashMap<>();
		Queue<Product> productQueue = new ArrayDeque<>();

		Product product1 = new CocaCola();
		product1.setId(1);
		product1.setPrice(3.5);
		Product product2 = new CocaCola();
		product2.setId(2);
		productQueue.add(product1);
		productQueue.add(product2);
		storageMap.put(3, productQueue);

		ControlPanel controlPanel = new ControlPanel();
		controlPanel.setiVendingMachine(iVendingMachine);

		storage.setStorageMap(storageMap);
		iVendingMachine.setStorage(storage);

		//Product dispenceProduct = controlPanel.dispenceProduct(3);
		PDFGenerator pdfGenerator = new PDFGenerator();
		pdfGenerator.generatePDF(productQueue);
		EmailSender emailSender = new EmailSender();
		emailSender.sendEmail("beatrice.a99@yahoo.com");
		// presupune ca nu e nul
		//assertNotNull(dispenceProduct);
		//assertEquals(1, dispenceProduct.getId());

	}
}
