package com.vendingmachine.bank;

import java.util.Queue;

public class Bank {
	private int id;
	private Queue<Money> tenCentsStack;
	private Queue<Money> fiftyCentsStack;
	private Queue<Money> oneDollarsStack;
	private Queue<Money> fiveDollarsStack;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Queue<Money> getTenCentsStack() {
		return tenCentsStack;
	}

	public void setTenCentsStack(Queue<Money> tenCentsStack) {
		this.tenCentsStack = tenCentsStack;
	}

	public Queue<Money> getFiftyCentsStack() {
		return fiftyCentsStack;
	}

	public void setFiftyCentsStack(Queue<Money> fiftyCentsStack) {
		this.fiftyCentsStack = fiftyCentsStack;
	}

	public Queue<Money> getOneDollarsStack() {
		return oneDollarsStack;
	}

	public void setOneDollarsStack(Queue<Money> oneDollarsStack) {
		this.oneDollarsStack = oneDollarsStack;
	}

	public Queue<Money> getFiveDollarsStack() {
		return fiveDollarsStack;
	}

	public void setFiveDollarsStack(Queue<Money> fiveDollarsStack) {
		this.fiveDollarsStack = fiveDollarsStack;
	}

}
