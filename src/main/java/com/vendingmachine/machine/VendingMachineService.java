package com.vendingmachine.machine;

import java.util.ArrayDeque;
import java.util.Map;
import java.util.Queue;

import com.vendingmachine.bank.Money;
import com.vendingmachine.exception.NotFullPaidException;
import com.vendingmachine.exception.SoldOutException;
import com.vendingmachine.machine.product.Product;
import com.vendingmachine.machine.storage.Storage;

public class VendingMachineService implements IVendingMachine {

	private Storage storage;

	public Storage getStorage() {
		return storage;
	}

	public void setStorage(Storage storage) {
		this.storage = storage;
	}

	@Override
	public Product dispenceProduct(int shelfCode) {
		Map<Integer, Queue<Product>> storageMap = storage.getStorageMap();
		Queue<Product> productQueue = storageMap.get(shelfCode);
		if (productQueue.isEmpty()) {
			throw new SoldOutException("Product is not available.");
		}
		Product head = productQueue.poll();
		return head;
	}

	@Override
	public boolean payProductPrice(int shelfCode, double amountPaid) {
		Map<Integer, Queue<Product>> storageMap = storage.getStorageMap();
		Queue<Product> productQueue = storageMap.get(shelfCode);
		double productPrice = productQueue.peek().getPrice();

		if (amountPaid >= productPrice) {
			// return rest to do

			dispenceProduct(shelfCode);
			return true;
		} else {
			throw new NotFullPaidException("The amount of money introduced is insufficient");
		}

	}

	@Override
	public Queue<Money> returnChange(double amountReceived, double amountExpected) {
		// amountReceived=11
		// amountExpected =6
		// change = 5
		// fisrtCount = 5%5 = 0
		// 1 bancnota = 5/5
		double totalChange = amountReceived - amountExpected;
		double change = totalChange % 5;
		double nrOfCoins = totalChange / 5;
		Queue<Money> coins = new ArrayDeque<Money>();
		if (nrOfCoins != 0) {
			// create queue of coins to return
			for (int i = 0; i <= nrOfCoins; i++) {
				coins.add(Money.FIVE_DOLLAR);
			}

		}
		if (change == 0) {
			return coins;
		} else {
			change = change % 1;
			nrOfCoins = change / 1;
			if (nrOfCoins != 0) {
				for (int i = 0; i <= nrOfCoins; i++) {
					coins.add(Money.ONE_DOLLAR);
				}
			}
			if (change == 0) {
				return coins;
			} else {
				change = change % 0.5;
				nrOfCoins = change / 0.5;
				if (nrOfCoins != 0) {
					for (int i = 0; i <= nrOfCoins; i++) {
						coins.add(Money.FIFTY_CENT);
					}
				}
				if (change == 0) {
					return coins;
				} else {
					change = change % 0.1;
					nrOfCoins = change / 0.1;
					if (nrOfCoins != 0) {
						for (int i = 0; i <= nrOfCoins; i++) {
							coins.add(Money.TEN_CENT);
						}
					}
					if (change == 0) {
						return coins;
					}
				}

			}
			return coins;

		}
	}

	@Override
	public boolean validateAmountReceived(Queue<Money> moneyRecived) {
		//parcurgem queue-ul si tot ce e diferit de banii pe care noi ii acceptam, respingem
		return false;
	}
}

//to do metoda care verifica in storage dc produsul e disponibil
//to do met care verifica in bank daca avem rest disponibil
// yo do  met care verifica banii introdusi de useer 
