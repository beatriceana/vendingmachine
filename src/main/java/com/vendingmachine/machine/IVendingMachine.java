package com.vendingmachine.machine;

import java.util.Queue;

import com.vendingmachine.bank.Money;
import com.vendingmachine.machine.product.Product;

public interface IVendingMachine {
	public Product dispenceProduct(int shelfCode);

	public boolean payProductPrice(int shelfCode, double amountPaid);

	public Queue<Money> returnChange(double amountReceived, double amountExpected);
	public boolean validateAmountReceived(Queue<Money> moneyRecived);

}
