package com.vendingmachine.controlpanel;

import com.vendingmachine.machine.IVendingMachine;
import com.vendingmachine.machine.product.Product;

public class ControlPanel {

	private IVendingMachine iVendingMachine;

	public IVendingMachine getiVendingMachine() {
		return iVendingMachine;
	}

	public void setiVendingMachine(IVendingMachine iVendingMachine) {
		this.iVendingMachine = iVendingMachine;
	}

	public Product dispenceProduct(int shelfCode) {
		// am apelat metoda din vendingmachine
		return iVendingMachine.dispenceProduct(shelfCode);
	}
}
